from flask import url_for

from workout.models import Gym, db


def test_create(client):
    url = url_for('gym.create_gym')
    response = client.post(url, json={'name': 'test-gym'})
    assert response.status_code == 201

    gym_id = response.get_json()['gym']['id']

    gym = db.session.query(Gym).get(gym_id)
    assert gym.name == 'test-gym'


def test_conflict(client):
    gym = Gym(name='test-gym')
    db.session.add(gym)
    db.session.commit()
    url = url_for('gym.create_gym')
    response = client.post(url, json={'name': gym.name})
    assert response.status_code == 409

