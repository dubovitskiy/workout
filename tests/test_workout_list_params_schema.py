import pytest

from workout.views.workouts import ListParamsSchema

empty = '<empty>'


@pytest.mark.parametrize('field,given,expected', [
    ['after_id', empty, None],
    ['after_id', 'test', None],
    ['after_id', 0, None],
    ['after_id', 1, 1],
    ['order_by', None, 'id'],
    ['order_by', empty, 'id'],
    ['order_by', 'test', 'id'],
    ['order_by', 'id', 'id'],
    ['order_by', 1, 'id'],
    ['order_by', '-1', 'id'],
    ['order', empty, 'desc'],
    ['order', None, 'desc'],
    ['order', 1, 'desc'],
    ['order', '1', 'desc'],
    ['order', 'test', 'desc'],
    ['order', 'desc', 'desc'],
    ['order', 'asc', 'asc'],
    ['limit', empty, 10],
    ['limit', None, 10],
    ['limit', -1, 10],
    ['limit', 0, 10],
    ['limit', 'test', 10],
    ['limit', 101, 10],
    ['limit', 99, 99],
])
def test_schema(field, given, expected):
    schema = ListParamsSchema(only=[field])

    if given == empty:
        data, err = schema.load({})
    else:
        data, err = schema.load({field: given})

    assert not err
    assert data[field] == expected
