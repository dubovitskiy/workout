import pytest

from workout import create_app
from workout.models import db


@pytest.yield_fixture(autouse=True)
def app():
    wk = create_app()
    with wk.app_context():
        db.create_all()

    yield wk

    with wk.app_context():
        db.drop_all()
