import json
from flask import url_for

from workout.models import (
    db,
    Muscle,
    Inventory,
    Gym,
    Exercise)


expected_workout_template = """{
    "exercises": [
        {
            "exercise": {
                "id": 1,
                "inventory": [
                    {
                        "id": 1,
                        "name": "bench"
                    }
                ],
                "name": "test-1",
                "other_muscles": [],
                "target_muscles": [
                    {
                        "id": 2,
                        "name": "triceps"
                    }
                ]
            },
            "id": 1,
            "repeats": 10,
            "sets": 4
        },
        {
            "exercise": {
                "id": 2,
                "inventory": [],
                "name": "test-2",
                "other_muscles": [
                    {
                        "id": 1,
                        "name": "biceps"
                    }
                ],
                "target_muscles": [
                    {
                        "id": 2,
                        "name": "triceps"
                    }
                ]
            },
            "id": 2,
            "repeats": 5,
            "sets": 2
        }
    ],
    "gym": {
        "id": 1,
        "name": "gold-gym"
    },
    "gym_id": 1,
    "id": 1
}"""


def test_create(client):
    biceps = Muscle(name='biceps')
    triceps = Muscle(name='triceps')
    bench = Inventory(name='bench')
    gym = Gym(name='gold-gym')

    db.session.add_all([biceps, triceps, bench, gym])
    db.session.commit()

    exercise_1 = Exercise(
        name='test-1',
        target_muscles=[triceps],
        inventory=[bench]
    )

    exercise_2 = Exercise(
        name='test-2',
        target_muscles=[triceps],
        other_muscles=[biceps]
    )

    db.session.add_all([exercise_1, exercise_2])
    db.session.commit()

    create_url = url_for('workouts.create_workout')

    request_data = {
        'gym_id': gym.id,
        'exercises': [
            {
                'sets': 4,
                'repeats': 10,
                'exercise_id': exercise_1.id
            },
            {
                'sets': 2,
                'repeats': 5,
                'exercise_id': exercise_2.id
            }
        ]
    }

    response = client.post(create_url, json=request_data)
    assert response.status_code == 201
    stringified = json.dumps(response.json, indent=4)
    assert stringified == expected_workout_template


def test_list(client):
    list_url = url_for('workouts.workouts_list')
    response = client.get(list_url)
    assert response.status_code == 200

