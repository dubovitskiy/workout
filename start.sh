#!/usr/bin/env bash

docker build \
    --tag workout \
    --no-cache . && \

docker run \
    --rm \
    --publish 5000:5000 \
    workout