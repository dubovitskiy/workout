from datetime import datetime

import click
from flask.cli import AppGroup

from workout import password_manager
from workout.models import db, User

passman = password_manager.Md5PassManager()

user_cli = AppGroup('user')


@user_cli.command('create')
def create_user():
    email = click.prompt('Email')
    password = click.prompt(
        'Password',
        hide_input=True,
        confirmation_prompt=True
    )
    user = User(
        email=email,
        password=passman.hash_password(password),
        email_confirmed_at=datetime.utcnow()
    )
    db.session.add(user)
    db.session.commit()

@user_cli.command('delete')
@click.argument('user_id')
def remove_user(user_id):
    user = db.session.query(User).get(user_id)
    if not user:
        return

    db.session.delete(user)
    db.session.commit()
