from flask import Blueprint, jsonify, request
from marshmallow import Schema, fields, post_load
from werkzeug.exceptions import Conflict, NotFound, BadRequest

from workout.models import Gym, db, Workout

gym_bp = Blueprint('gym', __name__, url_prefix='/api/v1/gyms')


class GymSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)

    @post_load
    def _load_gym(self, gym_data):
        return Gym(**gym_data)


schema = GymSchema()


@gym_bp.route('/', methods=['GET'])
def gyms_list():
    gyms = db.session.query(Gym).all()
    result = schema.dump(gyms, many=True)
    return jsonify({'gyms': result.data})


@gym_bp.route('/', methods=['POST'])
def create_gym():
    gym, errors = schema.load(request.get_json(force=True))
    if errors:
        raise BadRequest()
    print(gym)
    similar = db.session.query(Gym) \
        .filter_by(name=gym.name) \
        .count()

    if similar:
        raise Conflict()

    db.session.add(gym)
    db.session.flush()
    result = schema.dump(gym)
    db.session.commit()
    return jsonify({'gym': result.data}), 201


@gym_bp.route('/<int:gym_id>', methods=['DELETE'])
def delete_gym(gym_id):
    gym = db.session.query(Gym).get(gym_id)
    if not gym:
        raise NotFound()

    workouts = db.session.query(Workout) \
        .filter_by(gym_id=gym_id).count()

    if workouts:
        raise Conflict()

    db.session.delete(gym)
    db.session.commit()
    return jsonify({}), 204
