from flask import Blueprint, jsonify
from marshmallow import Schema, fields

from workout.models import (
    db,
    Muscle,
)

muscles_bp = Blueprint('muscles', __name__, url_prefix='/api/v1/muscles')


class MuscleSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)


schema = MuscleSchema()


@muscles_bp.route('/', methods=['GET'])
def muscles_list():
    muscles = db.session.query(Muscle)
    result = schema.dump(muscles, many=True)
    return jsonify({'muscles': result.data})
