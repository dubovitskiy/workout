from flask import (
    Blueprint,
    request,
    jsonify
)
from marshmallow import (
    Schema,
    fields,
    post_load,
    validate as v,
)
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
)

from workout.models import (
    db,
    User,
)

from workout import password_manager


passman = password_manager.Md5PassManager()
users_bp = Blueprint('users', __name__, url_prefix='/api/v1/users')


class RegisterSchema(Schema):
    email = fields.String(required=True, validators=[v.Email()])
    password = fields.String(required=True)

    @post_load
    def _create_user(self, valid_data):
        return User(
            email=valid_data['email'],
            password=passman.hash_password(valid_data['password'])
        )

register_schema = RegisterSchema()


@users_bp.route('')
def register():
    user, err = register_schema.load(request.get_json(force=True))
    if err:
        raise BadRequest(err)

    same_user = db.session.query(User) \
        .filter_by(email=user.email) \
        .exists()

    if same_user:
        raise Conflict()

    db.session.add(user)
    db.session.commit()
    return jsonify({})
