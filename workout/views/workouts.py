from flask import Blueprint, jsonify, request
from marshmallow import (
    Schema,
    fields,
    post_load,
    validate as v,
    ValidationError
)
from sqlalchemy.orm import joinedload
from werkzeug.exceptions import BadRequest

from workout.models import Workout, WorkoutExercise, db, Exercise

workouts_bp = Blueprint('workouts', __name__, url_prefix='/api/v1/workouts')


class BaseSchema(Schema):
    def force_load(self, *args, **kwargs):
        obj, err = self.load(*args, **kwargs)
        if err:
            raise BadRequest(err)
        return obj


class MuscleSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)


class InventorySchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)


class ExerciseSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)
    target_muscles = fields.Nested(MuscleSchema, many=True)
    other_muscles = fields.Nested(MuscleSchema, many=True)
    inventory = fields.Nested(InventorySchema, many=True)


class WorkoutExerciseSchema(BaseSchema):
    id = fields.Integer(dump_only=True)
    sets = fields.Integer(required=False, default=1)
    repeats = fields.Integer(required=False, default=10)
    exercise_id = fields.Integer(required=True)
    exercise = fields.Nested(ExerciseSchema)

    @post_load
    def _create_workout_exercise(self, data):
        return WorkoutExercise(**data)


class GymSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)


class WorkoutSchema(BaseSchema):
    id = fields.Integer(dump_only=True)
    gym_id = fields.Integer(required=False)
    exercises = fields.Nested(WorkoutExerciseSchema, many=True)
    gym = fields.Nested(GymSchema, required=False)

    @post_load
    def _create_workout(self, workout_dict):
        return Workout(**workout_dict)


load_schema = WorkoutSchema()
dump_schema = WorkoutSchema(
    exclude=[
        'exercises.exercise_id'
    ]
)


class DefaultMixin:
    def deserialize(self, value, attr=None, data=None):
        try:
            result = super().deserialize(value, attr, data)
        except ValidationError:
            result = self.default

        return result

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.missing = kwargs['default']


class DefaultInteger(DefaultMixin, fields.Integer):
    pass


class DefaultString(DefaultMixin, fields.String):
    pass


class ListParamsSchema(Schema):
    after_id = DefaultInteger(
        default=None,
        validate=[v.Range(min=1)]
    )
    order_by = DefaultString(
        default='id',
        validate=[v.OneOf(['id'])]  # TODO: add date
    )
    order = DefaultString(
        default='desc',
        validate=[v.OneOf(['desc', 'asc'])]
    )
    limit = DefaultInteger(
        default=10,
        validate=v.Range(1, 100)
    )


list_args_schema = ListParamsSchema()


@workouts_bp.route('', methods=['GET'])
def workouts_list():
    args, _ = list_args_schema.load(request.args)

    limit = args['limit']

    order_by = getattr(Workout, args['order_by'])
    order_attr = getattr(order_by, args['order'])

    workouts = db.session.query(Workout) \
        .options(joinedload(Workout.gym)) \
        .options(joinedload(Workout.exercises)) \
        .options(joinedload(Workout.exercises).
                 joinedload(WorkoutExercise.exercise).
                 joinedload(Exercise.inventory)) \
        .options(joinedload(Workout.exercises)
                 .joinedload(WorkoutExercise.exercise)
                 .joinedload(Exercise.target_muscles)) \
        .options(joinedload(Workout.exercises)
                 .joinedload(WorkoutExercise.exercise)
                 .joinedload(Exercise.other_muscles)) \
        .order_by(order_attr()) \
        .limit(limit)

    result = dump_schema.dump(workouts, many=True)
    return jsonify({'workouts': result.data})


@workouts_bp.route('', methods=['POST'])
def create_workout():
    workout = load_schema.force_load(request.get_json(force=True))

    exercise_ids = {e.exercise_id for e in workout.exercises}
    existing_exercises_count = db.session.query(Exercise) \
        .filter(Exercise.id.in_(exercise_ids)) \
        .count()

    if existing_exercises_count != len(exercise_ids):
        db.session.rollback()
        raise BadRequest()

    db.session.add(workout)
    db.session.commit()

    workout = db.session.query(Workout) \
        .filter_by(id=workout.id) \
        .options(joinedload(Workout.gym)) \
        .options(joinedload(Workout.exercises)) \
        .options(joinedload(Workout.exercises).
                 joinedload(WorkoutExercise.exercise).
                 joinedload(Exercise.inventory)) \
        .options(joinedload(Workout.exercises)
                 .joinedload(WorkoutExercise.exercise)
                 .joinedload(Exercise.target_muscles)) \
        .options(joinedload(Workout.exercises)
                 .joinedload(WorkoutExercise.exercise)
                 .joinedload(Exercise.other_muscles)) \
        .one()
    result = dump_schema.dump(workout)
    return jsonify(result.data), 201
