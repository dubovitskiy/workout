from .gym import gym_bp as gym
from .workouts import workouts_bp as workouts
from .muscles import muscles_bp as muscles
from .exercises import exercises_bp as exercises
from .users import users_bp as users
