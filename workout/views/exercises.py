from flask import Blueprint, jsonify
from marshmallow import Schema, fields

from workout.models import (
    db,
    Exercise,
)

exercises_bp = Blueprint('exercises', __name__, url_prefix='/api/v1/exercises')


class MuscleSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)


class InventorySchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)


class ExerciseSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)
    target_muscles = fields.Nested(MuscleSchema, many=True)
    other_muscles = fields.Nested(MuscleSchema, many=True)
    inventory = fields.Nested(InventorySchema, many=True)


schema = ExerciseSchema()


@exercises_bp.route('/', methods=['GET'])
def exercises_list():
    exercises = db.session.query(Exercise)
    result = schema.dump(exercises, many=True)
    return jsonify({'exercises': result.data})
