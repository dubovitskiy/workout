import os

from flask import Flask

from workout.models import db
from workout import views, cli, plugins


def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('WORKOUT_DATABASE_URI')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    # app.config['SQLALCHEMY_ECHO'] = True
    db.init_app(app)

    app.register_blueprint(views.gym)
    app.register_blueprint(views.workouts)
    app.register_blueprint(views.muscles)
    app.register_blueprint(views.exercises)
    app.register_blueprint(views.users)

    app.cli.add_command(cli.user_cli)

    plugins.jwt.init_app(app)
    plugins.migrator.init_app(app, db)
    return app
