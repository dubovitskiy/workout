import hashlib


class AbstractPassManager:
    def hash_password(self, password):
        raise NotImplementedError()

    def password_valid(self, password, password_hash):
        raise NotImplementedError()

    def init_app(self, app):
        pass


class Md5PassManager(AbstractPassManager):
    def hash_password(self, password):
        hashed = hashlib.md5(password.encode())
        return hashed.hexdigest()

    def password_valid(self, password, password_hash):
        hashed = self.hash_password(password)
        return hashed == password_hash
