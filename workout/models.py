from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    is_active = db.Column(db.Boolean(), nullable=False, server_default='1')

    email = db.Column(db.String(255, collation='NOCASE'), nullable=False, unique=True)
    email_confirmed_at = db.Column(db.DateTime())
    password = db.Column(db.String(255), nullable=False, server_default='')

    first_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    last_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')

    roles = db.relationship('Role', secondary='user_roles')

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True)


class UserRoles(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))


class Muscle(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))

    __tablename__ = 'muscles'


class Inventory(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))

    __tablename__ = 'inventory'


class ExerciseTargetMuscle(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id'))
    muscle_id = db.Column(db.Integer, db.ForeignKey('muscles.id'))

    __tablename__ = 'exercise_to_target_muscle'


class ExerciseOtherMuscle(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id'))
    muscle_id = db.Column(db.Integer, db.ForeignKey('muscles.id'))

    __tablename__ = 'exercise_to_other_muscle'


class ExerciseInventory(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id'))
    inventory_id = db.Column(db.Integer, db.ForeignKey('inventory.id'))

    __tablename__ = 'exercise_to_inventory'


class GymInventory(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    gym_id = db.Column(db.Integer, db.ForeignKey('gyms.id'))
    inventory_id = db.Column(db.Integer, db.ForeignKey('inventory.id'))

    __tablename__ = 'gym_to_inventory'


class Exercise(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))
    target_muscles = db.relationship('Muscle', secondary='exercise_to_target_muscle')
    other_muscles = db.relationship('Muscle', secondary='exercise_to_other_muscle')
    inventory = db.relationship('Inventory', secondary='exercise_to_inventory')

    __tablename__ = 'exercises'


class Gym(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))
    inventory = db.relationship('Inventory', secondary='gym_to_inventory')

    __tablename__ = 'gyms'


class Workout(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    gym_id = db.Column(db.Integer, db.ForeignKey('gyms.id'), nullable=True)
    gym = db.relationship('Gym')
    exercises = db.relationship('WorkoutExercise', back_populates='workout')
    __tablename__ = 'workouts'


class WorkoutExercise(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    sets = db.Column(db.Integer, default=1)
    repeats = db.Column(db.Integer, default=1)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id'))
    workout_id = db.Column(db.Integer, db.ForeignKey('workouts.id'))
    exercise = db.relationship('Exercise')
    workout = db.relationship('Workout', back_populates='exercises')

    __tablename__ = 'workout_exercises'

