FROM python:3.7.2-alpine3.8

ENV FLASK_ENV=development
ENV FLASK_APP=workout:app
ENV FLASK_DEBUG=1
ENV CODE_DIR=/code
ENV WORKOUT_DATABASE_URI=sqlite:////var/tmp/wk.db

COPY . "${CODE_DIR}/"

RUN pip install --requirement ${CODE_DIR}/requirements.txt

WORKDIR "${CODE_DIR}"
CMD sh -c "flask db upgrade && flask run --host 0.0.0.0 --port 5000"
